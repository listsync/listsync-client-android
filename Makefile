all: test

test:
	./scripts/update-local-properties
	./gradlew test

connectedAndroidTest:
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-21
	./gradlew connectedAndroidTest
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-29
	./gradlew connectedAndroidTest

connectedAndroidTest-21:
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-21
	./gradlew connectedAndroidTest


connectedAndroidTest-29:
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-29
	./gradlew connectedAndroidTest

setup:
	sudo apt install openjdk-11-jdk
	./scripts/install-android-tools
