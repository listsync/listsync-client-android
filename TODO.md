# TODO

## List view

- [x] Checkbox and TextView
- [x] Re-order items
- [x] Drag handle for re-ordering
- [ ] Add new items via ActionBar button
- [ ] Add new items by pressing return?
- [ ] Save in local DB
- [ ] Initial load from API
- [ ] Load/save to/from API: perform all my queued actions, then pull
      (and compare?)
- [ ] Display API errors
- [ ] Display pending sync actions
- [ ] Create new list
- [ ] Server settings
- [ ] Preferences


