package net.artificialworlds.listsync

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import net.artificialworlds.listsync.data.Item
import net.artificialworlds.listsync.databinding.FragmentItemListBinding

class ListFragment : Fragment(), OnStartDragListener {
    private lateinit var viewModel: ListViewModel
    private lateinit var binding: FragmentItemListBinding
    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var itemRecyclerViewAdapter: ListRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(ListViewModel::class.java)
        viewModel.title.observe(
            viewLifecycleOwner,
            Observer { title -> activity?.title = title })

        binding = FragmentItemListBinding.inflate(inflater, container, false)
        binding.list.layoutManager = LinearLayoutManager(context)

        itemRecyclerViewAdapter =
            ListRecyclerViewAdapter(viewModel.items, this)
        binding.list.adapter = itemRecyclerViewAdapter

        itemTouchHelper =
            ItemTouchHelper(ItemTouchCallbacks(itemRecyclerViewAdapter))
        itemTouchHelper.attachToRecyclerView(binding.list)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_item -> {
                val pos = itemRecyclerViewAdapter.addItem(
                    Item(
                        10.0,
                        true,
                        "NEW item"
                    )
                )
                binding.list.scrollToPosition(pos)
                true
            }
            R.id.new_list -> {
                viewModel.title.value = "new clicked"
                true
            }
            R.id.switch_list -> {
                viewModel.title.value = "Changed"
                true
            }
            R.id.settings -> TODO()
            R.id.exit -> TODO()
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        itemTouchHelper.startDrag(viewHolder)
    }
}
