package net.artificialworlds.listsync

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup

import kotlinx.android.synthetic.main.fragment_item.view.*
import net.artificialworlds.listsync.data.Item
import net.artificialworlds.listsync.data.Items

class ListRecyclerViewAdapter(
    private val items: Items,
    private val onStartDragListener: OnStartDragListener
) : RecyclerView.Adapter<ListRecyclerViewAdapter.ViewHolder>(),
    ItemTouchHelperAdapter {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    //! Keeps track of which position currently has focus
    //! TODO: surely we don't need to keep track of this manually?
    private var focussedPosition: Int = -1

    //! If someone sets this, we request focus for the item at this position
    private var requestedFocusPosition: Int? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(
        holder: ListRecyclerViewAdapter.ViewHolder,
        position: Int
    ) {
        val item = items.value[position]
        holder.itemView.itemCheckBox.isChecked = item.ticked
        holder.itemView.itemEditText.setText(item.text)

        // This is not accessible since it uses touch events rather than
        // clicks, but rows may be re-ordered using drag and drop on the
        // whole row item - the drag handle is just a convenience feature.
        holder.itemView.itemDragHandle.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                onStartDragListener.onStartDrag(holder)
            }
            false
        }
    }

    override fun getItemCount(): Int = items.value.size

    override fun onViewAttachedToWindow(
        holder: ListRecyclerViewAdapter.ViewHolder
    ) {
        super.onViewAttachedToWindow(holder)
        holder.itemView.itemEditText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                focussedPosition = holder.adapterPosition
            }
        }
        if (holder.adapterPosition == requestedFocusPosition) {
            holder.itemView.requestFocus()
            holder.itemView.itemEditText.selectAll()
            requestedFocusPosition = null
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        items.value.add(toPosition, items.value.removeAt(fromPosition))
        notifyItemMoved(fromPosition, toPosition)
        when {
            fromPosition == focussedPosition -> {
                focussedPosition = toPosition
            }
            focussedPosition in fromPosition..toPosition -> {
                focussedPosition--
            }
            focussedPosition in toPosition until fromPosition -> {
                focussedPosition++
            }
        }
        return true
    }

    override fun onItemDismiss(position: Int) {
        items.value.removeAt(position)
        notifyItemRemoved(position)
        if (focussedPosition > position) {
            focussedPosition--
        } else if (focussedPosition == position) {
            focussedPosition = -1
        }
    }

    fun addItem(item: Item): Int {
        val pos = focussedPosition + 1
        items.value.add(pos, item)
        notifyItemInserted(pos)
        requestedFocusPosition = pos
        return pos
    }
}
