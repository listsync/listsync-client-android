package net.artificialworlds.listsync

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import net.artificialworlds.listsync.data.Items
import net.artificialworlds.listsync.data.ListInfo
import net.artificialworlds.listsync.data.ListPath
import net.artificialworlds.listsync.db.ItemDao
import net.artificialworlds.listsync.db.ListInfoDao
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class Repository(
    private val server: Server,
    private val executor: Executor,
    private val itemDao: ItemDao,
    private val listInfoDao: ListInfoDao
) {

    fun saveListInfo(listPath: ListPath, listInfo: ListInfo) {
        /*executor.execute {
            server.putListInfo(listPath.username, listPath.listid, listInfo)
        }*/
        listInfoDao.save(listInfo)
    }

    fun loadListInfo(listPath: ListPath): LiveData<ListInfo> {
        refreshListInfo(listPath)
        val ret = listInfoDao.load(listPath)
        return if (ret.value == null) {
            MutableLiveData(ListInfo(listPath, "New list"))
        } else {
            ret
        }
    }

    private fun refreshListInfo(listPath: ListPath) {
        /*executor.execute {
            // TODO: check for freshness?  How?
            if (listInfoDao.load(listPath).value == null) {
                val response = server.getListInfo(
                    listPath.username,
                    listPath.listid
                ).execute()

                // TODO: Check for errors here, then remove !!
                listInfoDao.save(response.body()!!)
            }
        }*/
    }

    companion object {
        val FRESH_TIMEOUT = TimeUnit.DAYS.toMillis(1)
    }

    /*fun items(listPath: ListPath): LiveData<Items> {
        // This isn't an optimal implementation. We'll fix it later.
        val data = MutableLiveData<Items>()
        server.fetchItems(listPath.username.value, listPath.listid)
            .enqueue(object :
                Callback<Items> {
                override fun onResponse(
                    call: Call<Items>,
                    response: Response<Items>
                ) {
                    data.value = response.body()
                }

                override fun onFailure(call: Call<Items>, t: Throwable) {
                    TODO()
                }
            })
        return data
    }*/
}
