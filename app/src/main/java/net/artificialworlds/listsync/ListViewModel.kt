package net.artificialworlds.listsync

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.artificialworlds.listsync.data.*

class ListViewModel : ViewModel() {

    /*val username = "name"
    val listid = "id"*/
    val title = MutableLiveData<String>()
    val items: Items = Items(
        (1..20).map { i: Int ->
            Item(
                i * 16.0,
                false,
                "Item $i"
            )
        }.toMutableList()
    )

    init {
        title.value = "My Title"
    }
}

/*class ListViewModel(
    val repository: Repository,
    val listPath: ListPath?
) : ViewModel() {
    val listInfo = listInfo(repository, listPath)
    // TODO: get a SavedInstanceState from somewhere and use it?
}

private fun listInfo(
    listRepository: Repository,
    listPath: ListPath?
): LiveData<ListInfo> {
    return if (listPath != null) {
        listRepository.loadListInfo(listPath)
    } else {
        return MutableLiveData(
            ListInfo(
                listPath = ListPath("", ""),
                title = ""
            )
        )
    }
}*/

/*private fun items(
    listRepository: Repository,
    listPath: ListPath?
): LiveData<Items> {
    return if (listPath != null) {
        listRepository.items(listPath)
    } else {
        MutableLiveData(
            Items(emptyList())
        )
    }
}*/
