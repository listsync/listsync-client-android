package net.artificialworlds.listsync

import android.content.Context
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import net.artificialworlds.listsync.data.ListPath
//import net.artificialworlds.listsync.db.Db
import retrofit2.Retrofit
import java.util.concurrent.Executors

const val DEFAULT_SERVER_URL = "https://listsync.artificialworlds.net/api/"

/**
 * The singleton that holds all the stuff you should make in the main
 * method, but we don't have a main method.  If you change the server
 * URL, we re-create everything.
 */
object Factories {

    /*private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private var repository: Repository? = null
    private var database: Db? = null
    private var server: Server? = null
    private var listViewModels: HashMap<String, ListViewModel> = HashMap()

    private var serverUrl = DEFAULT_SERVER_URL
        set(value) {
            repository = null
            database = null
            server = null
            listViewModels.clear()
            field = value
        }

    @Synchronized
    fun repositoryInstance(applicationContext: Context): Repository {
        val ret = repository ?: Repository(
            serverInstance(),
            Executors.newCachedThreadPool(),
            databaseInstance(applicationContext).itemDao(),
            databaseInstance(applicationContext).listInfoDao()
        )
        repository = ret
        return ret
    }

    @Synchronized
    fun databaseInstance(applicationContext: Context): Db {
        val ret =
            database ?: Room.databaseBuilder(
                applicationContext,
                Db::class.java,
                "Db"
            ).build()
        database = ret
        return ret
    }

    @Synchronized
    fun serverInstance(): Server {
        // TODO: catch error is URL is bad
        val ret: Server = server ?: Retrofit.Builder()
            .baseUrl(serverUrl)
            .build()
            .create(Server::class.java)

        server = ret
        return ret
    }

    @Synchronized
    fun listViewModelInstance(
        applicationContext: Context,
        listPath: ListPath
    ): ListViewModel {
        return listViewModels.getOrPut(
            listPath.toString(),
            {
                ListViewModel(
                    repositoryInstance(applicationContext),
                    listPath
                )
            }
        )
    }

    fun <T> fromJson(string: String?, clazz: Class<T>): T? {
        return string?.let { moshi.adapter(clazz).fromJson(it) }
    }

    fun <T: Any> toJson(value: T): String {
        return moshi.adapter<T>(value::class.java).toJson(value)
    }*/
}
