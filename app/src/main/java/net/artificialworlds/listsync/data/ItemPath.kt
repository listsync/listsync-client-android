package net.artificialworlds.listsync.data

data class ItemPath(val listPath: ListPath, val itemId: String)
