package net.artificialworlds.listsync.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ListInfo(
    @PrimaryKey
    val listPath: ListPath,
    var title: String)
