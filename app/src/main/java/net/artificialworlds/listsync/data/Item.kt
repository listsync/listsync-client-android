package net.artificialworlds.listsync.data

data class Item(
    val order: Double,
    val ticked: Boolean,
    var text: String
)