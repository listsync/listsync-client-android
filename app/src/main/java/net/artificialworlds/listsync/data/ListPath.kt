package net.artificialworlds.listsync.data

data class ListPath(val username: String, val listid: String)
