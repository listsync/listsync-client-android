package net.artificialworlds.listsync

import net.artificialworlds.listsync.data.Items
import net.artificialworlds.listsync.data.ListInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface Server {
    @GET("/v1/list/{username}/{listid}/items")
    fun fetchItems(
        @Path("username") username: String,
        @Path("listid") listid: String
    ): Call<Items>

    @GET("/v1/list/{username}/{listid}")
    fun getListInfo(
        @Path("username") username: String,
        @Path("listid") listid: String
    ): Call<ListInfo>

    @PUT("/v1/list/{username}/{listid}")
    fun putListInfo(
        @Path("username") username: String,
        @Path("listid") listid: String,
        listInfo: ListInfo
    ): Call<ListInfo>
}