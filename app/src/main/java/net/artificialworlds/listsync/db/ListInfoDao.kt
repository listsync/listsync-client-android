package net.artificialworlds.listsync.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import net.artificialworlds.listsync.data.ListInfo
import net.artificialworlds.listsync.data.ListPath

@Dao
interface ListInfoDao {
    @Insert(onConflict = REPLACE)
    fun save(listInfo: ListInfo)

    @Query("SELECT * FROM ListInfo WHERE listPath = :listPath")
    fun load(listPath: ListPath): LiveData<ListInfo>
}
