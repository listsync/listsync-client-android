package net.artificialworlds.listsync.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import net.artificialworlds.listsync.data.Item
import net.artificialworlds.listsync.data.ItemPath

@Dao
interface ItemDao {
    @Insert(onConflict = REPLACE)
    fun save(user: Item)

    @Query("SELECT * FROM Item WHERE itemPath = :itemPath")
    fun load(itemPath: ItemPath): LiveData<Item>
}
