package net.artificialworlds.listsync

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class ItemTouchCallbacks(
    private val itemRecyclerViewAdapter: ListRecyclerViewAdapter
) :
    ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN,
        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
    ) {
    override fun onMove(
        recyclerView: RecyclerView,
        dragged: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return itemRecyclerViewAdapter.onItemMove(
            dragged.adapterPosition,
            target.adapterPosition
        )
    }

    override fun onSwiped(
        target: RecyclerView.ViewHolder,
        direction: Int
    ) {
        itemRecyclerViewAdapter.onItemDismiss(
            target.adapterPosition
        )
    }
}
