listsync is a place to store your simple lists.

**This project was intended to be the Android app, but is now abandoned. Try https://tasks.org/**

## Before you build

To prepare your development environment, if you are running Ubuntu or Debian
or similar:

```bash
make setup
```

On other operating systems, install Java and then run:

```bash
./scripts/install-android-tools
```

## Build

To build and run tests:

```bash
make
```

<!-- TODO: not yet
## Packaging a new release

To build an APK:

```bash
make release
```

To upload the APK:

```bash
make deploy
```
-->

## Run tests on emulators

```bash
make connectedAndroidTest
```

This depends on a flakey pile of scripts and environment variables that
are inside the `scripts` directory.  Improvements to make those scripts
more reliable would be a welcome contribution to this project.

## Updating SDK

When Android Studio says you need to update some Android SDK components, run
this command:

```bash
./scripts/sdkmanager --update
```

## Android Studio

You should be able to import the gradle project into Android Studio, but we
avoid checking any Android Studio files in to git.  This helps us ensure the
command-line build is clean and able to run independently and reliably.

The build scripts overwrite the file `local.properties` inside the
listsync-client-android directory - this should help Android Studio find the
Android SDK files that are downloaded during the build.  To keep your
environment consistent with our automated builds, we suggest NOT using Android
Studio to download Android SDK components, but instead updating the script
we use to set things up: `scripts/install-android-tools`.

## Developer notes

### Updating Gradle

#### To upgrade gradle:

Edit gradle/wrapper/gradle-wrapper.properties and set the required
gradle version in distributionUrl.

#### To upgrade the android gradle plugin:

Edit build.gradle and in the dependencies section, set the required
gradle plugin version in the entry for
'com.android.tools.build:gradle'.

### Bootstrapping

This is what I ran to start the project from nothing:

```bash
sudo apt install gradle
gradle wrapper --gradle-version=6.3
sudo apt remove gradle
sudo apt autoremove

# I chose Application, Kotlin, Kotlin:
./gradlew init
```

Then I used Android Studio to generate a template project.
